package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void log(@NotNull final String json);

}
