package ru.t1.ytarasov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.task.TaskShowByProjectIdRequest;
import ru.t1.ytarasov.tm.dto.response.task.TaskShowByProjectIdResponse;
import ru.t1.ytarasov.tm.event.ConsoleEvent;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskShowByProjectIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @Nullable
    public static final String DESCRIPTION = "Show task by project id.";

    @Override
    @EventListener(condition = "@taskShowByProjectIdListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[FIND ALL TASKS BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @Nullable final TaskShowByProjectIdResponse response = getTaskEndpoint().showTaskByProjectId(request);
        @Nullable final List<TaskDTO> tasks = response.getTasks();
        for (TaskDTO task : tasks) System.out.println(task);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }
}
