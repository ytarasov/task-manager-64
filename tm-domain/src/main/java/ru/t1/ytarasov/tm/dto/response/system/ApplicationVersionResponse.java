package ru.t1.ytarasov.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ApplicationVersionResponse extends AbstractResponse {

    @Nullable
    private String version;

}
