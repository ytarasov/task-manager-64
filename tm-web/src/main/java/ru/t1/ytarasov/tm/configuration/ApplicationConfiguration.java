package ru.t1.ytarasov.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan("ru.t1.ytarasov.tm")
public class ApplicationConfiguration {
}
