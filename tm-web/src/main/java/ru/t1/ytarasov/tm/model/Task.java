package ru.t1.ytarasov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.ytarasov.tm.enumirated.Status;
import ru.t1.ytarasov.tm.util.DateUtil;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    private String description;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date created = new Date();

    @NotNull
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date updated = new Date();

    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateStart;

    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateFinish;

    private String projectId;

    public Task(@NotNull String name) {
        this.name = name;
    }

    public Task(@NotNull String name, String description) {
        this.name = name;
        this.description = description;
    }

}
