package ru.t1.ytarasov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ytarasov.tm.enumirated.Status;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.repository.ProjectRepository;
import ru.t1.ytarasov.tm.repository.TaskRepository;

@Controller
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @PostMapping("/task/create")
    public String create() {
        taskRepository.create();
        return "redirect:/tasks";
    }

    @GetMapping("task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Task task = taskRepository.findById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", projectRepository.findAll());
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute Task task, BindingResult result) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskRepository.save(task);
        return "redirect:/tasks";
    }



}
